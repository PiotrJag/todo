﻿# ToDo
This application is a task management system. It is multilingual and multiuser. This is web application and a REST API.

## Screenshots
![](./screenshots/screen0.png)

![](./screenshots/screen1.png)

## Technologies

* Java,
* Spring,
* MySQL,
* [Maven](https://maven.apache.org/)

## About the author

**Piotr Jagodziński**

* [piotrjagod@poczta.onet.pl](mailto:piotrjagod@poczta.onet.pl)

## License
[MIT](https://opensource.org/licenses/mit-license.php)