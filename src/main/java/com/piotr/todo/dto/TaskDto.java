package com.piotr.todo.dto;

import com.piotr.todo.entity.Priority;
import lombok.Data;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
public class TaskDto {

    private Long id;

    @Size(min = 3, max = 255)
    private String description;

    @FutureOrPresent
    private LocalDate finishDate;

    @NotNull
    private Priority priority;

    private Boolean finished;
}
