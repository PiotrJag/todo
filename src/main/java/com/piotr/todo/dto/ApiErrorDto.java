package com.piotr.todo.dto;

import lombok.Data;

@Data
public class ApiErrorDto {

    public String exceptionClass;
    public String message;
}
