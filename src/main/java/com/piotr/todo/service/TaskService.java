package com.piotr.todo.service;

import com.piotr.todo.dto.TaskDto;
import com.piotr.todo.entity.Task;
import com.piotr.todo.entity.User;
import com.piotr.todo.exception.EntityNotFoundException;
import com.piotr.todo.repository.TaskRepository;
import com.piotr.todo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final UserRepository userRepository;

    public List<TaskDto> findAll(String username) {
        return taskRepository.findAllByUserUsernameIgnoringCase(username).stream()
                .map(this::map).collect(Collectors.toList());
    }

    public TaskDto find(Long id, String username) {
        return taskRepository.findByIdAndUserUsernameIgnoringCase(id, username).map(this::map)
                .orElseThrow(() -> new EntityNotFoundException(Task.class, id));
    }

    @Transactional
    public TaskDto save(TaskDto task, String username) {
        if (task.getId() != null) {
            return update(task.getId(), task, username);
        }

        Task entity = map(task);
        User user = userRepository.findUserByUsernameIgnoringCase(username);
        entity.setUser(user);
        return map(taskRepository.save(entity));
    }

    @Transactional
    public TaskDto update(Long id, TaskDto task, String username) {
        Task entity = findEntity(id, username);
        entity.setDescription(task.getDescription());
        entity.setFinishDate(task.getFinishDate());
        entity.setFinished(task.getFinished());
        entity.setPriority(task.getPriority());
        return map(taskRepository.save(entity));
    }

    @Transactional
    public void delete(Long id, String username) {
        Task entity = findEntity(id, username);
        taskRepository.delete(entity);
    }

    private Task findEntity(Long id, String username) {
        return taskRepository.findByIdAndUserUsernameIgnoringCase(id, username)
                .orElseThrow(() -> new EntityNotFoundException(Task.class, id));
    }

    private TaskDto map(Task entity) {
        TaskDto dto = new TaskDto();
        dto.setId(entity.getId());
        dto.setDescription(entity.getDescription());
        dto.setFinishDate(entity.getFinishDate());
        dto.setPriority(entity.getPriority());
        dto.setFinished(entity.getFinished());
        return dto;
    }

    private Task map(TaskDto dto) {
        Task entity = new Task();
        entity.setId(dto.getId());
        entity.setDescription(dto.getDescription());
        entity.setFinishDate(dto.getFinishDate());
        entity.setPriority(dto.getPriority());
        entity.setFinished(dto.getFinished());
        return entity;
    }
}

