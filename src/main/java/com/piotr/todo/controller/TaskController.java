package com.piotr.todo.controller;

import com.piotr.todo.dto.TaskDto;
import com.piotr.todo.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RequiredArgsConstructor
@Controller
@RequestMapping({"/tasks", "/"})
public class TaskController {

    private final TaskService taskService;

    @GetMapping
    public String taskList(Model model, @RequestParam(required = false) Long deleteId, Principal principal) {
        if (deleteId != null) {
            taskService.delete(deleteId, principal.getName());
            return "redirect:tasks";
        }

        List<TaskDto> tasks = taskService.findAll(principal.getName());
        model.addAttribute("taskList", tasks);
        model.addAttribute("name", principal.getName());
        return "tasks";
    }

    @GetMapping("/{id}")
    public String editForm(@PathVariable Long id, Model model, Principal principal) {
        TaskDto task = taskService.find(id, principal.getName());
        model.addAttribute("taskToEdit", task);
        return "task_form";
    }

    @PostMapping
    public String saveTask(@ModelAttribute("task") TaskDto task, BindingResult errors, Principal principal) {
        if (errors.hasErrors()) {
            return "tasks";
        }

        taskService.save(task, principal.getName());

        return "redirect:tasks";
    }

    @ModelAttribute("task")
    public TaskDto task() {
        return new TaskDto();
    }
}
