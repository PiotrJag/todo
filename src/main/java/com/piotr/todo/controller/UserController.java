package com.piotr.todo.controller;

import com.piotr.todo.dto.UserDto;
import com.piotr.todo.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import javax.validation.Valid;

@RequiredArgsConstructor
@Controller
public class UserController {

    private final UserService userService;

    @GetMapping("/login")
    public String loginForm() {
        return "login";
    }

    @GetMapping("/register")
    public String registerForm() {
        return "register";
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("user") UserDto user, BindingResult errors) {
        if (errors.hasErrors()) {
            return "register";
        }

        userService.save(user);
        return "redirect:/login";
    }

    @ModelAttribute("user")
    public UserDto user() {
        return new UserDto();
    }
}
