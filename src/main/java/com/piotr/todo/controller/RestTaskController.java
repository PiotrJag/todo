package com.piotr.todo.controller;

import com.piotr.todo.dto.TaskDto;
import com.piotr.todo.service.TaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/tasks")
public class RestTaskController {

    private final TaskService taskService;

    @GetMapping
    public List<TaskDto> findAll(Principal principal) {
        return taskService.findAll(principal.getName());
    }

    @GetMapping("/{id}")
    public TaskDto find(@PathVariable Long id, Principal principal) {
        return taskService.find(id, principal.getName());
    }

    @PostMapping
    public TaskDto create(@Valid @RequestBody TaskDto task, Principal principal) {
        return taskService.save(task, principal.getName());
    }

    @PutMapping("/{id}")
    public TaskDto edit(@PathVariable Long id, @Valid @RequestBody TaskDto task, Principal principal) {
        return taskService.update(id, task, principal.getName());
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id, Principal principal) {
        taskService.delete(id, principal.getName());
    }
}
