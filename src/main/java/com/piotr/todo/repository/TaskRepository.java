package com.piotr.todo.repository;

import com.piotr.todo.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long> {

    List<Task> findAllByUserUsernameIgnoringCase(String username);

    Optional<Task> findByIdAndUserUsernameIgnoringCase(Long id, String username);
}
